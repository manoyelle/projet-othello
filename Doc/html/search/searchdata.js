var indexSectionsWithContent =
{
  0: "abcdfghlmnoprstvxy",
  1: "bclm",
  2: "acmt",
  3: "abcdfglmnprtv",
  4: "achnoptxy",
  5: "p",
  6: "bnv",
  7: "nost",
  8: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées",
  7: "Macros",
  8: "Pages"
};

