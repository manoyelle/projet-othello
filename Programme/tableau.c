#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "SDL2/SDL.h"
#include "tableau.h"
#include "constantes.h"

int **  genTableau (int ** origine,int x, int y, int joueur) {
	int autre = (joueur + 1) % 2;
	int ** res = creerTab();
	copieTab(origine,res);

	// Posistionnement du nouveau pion
	res[x][y] = joueur; 
	int autrePion, possible;
	int i,j;

	//Recherche des pions à retourner 

	// Diag haut gauche
	if ((x-1 > 0) && (y-1 > 0) && (origine[x-1][y-1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x-2;
		j = y-2;
		while (!autrePion && possible && i >= 0 && j >= 0) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i--;
				j--;
			}
		}
		if (autrePion) {
			i++;
			j++;
			while (i < x && j < y) {
				res[i][j] = joueur;
				i++;
				j++;
			}
		}
	}

	// Haut
	if ((y-1 > 0) && (origine[x][y-1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x;
		j = y-2;
		while (!autrePion && possible && j >= 0) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				j--;
			}
		}
		if (autrePion) {
			j++;
			while(j < y) {
				res[i][j] = joueur;
				j++;
			}
		}
	}

	// Diag haut droite
	if ((x+1 < 7) && (y-1 > 0) && (origine[x+1][y-1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x+2;
		j = y-2;
		while (!autrePion && possible && i <= 7 && j >= 0) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i++;
				j--;
			}
		}
		if (autrePion) {
			i--;
			j++;
			while (i > x && j < y) {
				res[i][j] = joueur;
				i--;
				j++;
			}
		}
	}

	// Gauche
	if ((x-1 > 0) && (origine[x-1][y] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x-2;
		j = y;
		while (!autrePion && possible && i >= 0) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i--;
			}
		}
		if (autrePion) {
			i++;
			while (i < x) {
				res[i][j] = joueur;
				i++;
			}
		}
	}

	// Droite
	if ((x+1 < 7) && (origine[x+1][y] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x+2;
		j = y;
		while (!autrePion && possible && i <= 7) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i++;
			}
		}
		if (autrePion) {
			i--;
			while (i > x) {
				res[i][j] = joueur;
				i--;
			}
		}
	}

	// Diag bas gauche
	if ((x-1 > 0) && (y+1 < 7) && (origine[x-1][y+1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x-2;
		j = y+2;
		while (!autrePion && possible && i >= 0 && j <= 7) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i--;
				j++;
			}
		}
		if (autrePion) {
			i++;
			j--;
			while (i < x && j > y) {
				res[i][j] = joueur;
				i++;
				j--;
			}
		}
	}

	// Bas 
	if ((y+1 < 7) && (origine[x][y+1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x;
		j = y+2;
		while (!autrePion && possible && j <= 7) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				j++;
			}
		}
		if (autrePion) {
			j--;
			while (j > y) {
				res[i][j] = joueur;
				j--;
			}
		}
	}

	// Diag bas droite
	if ((x+1 < 7) && (y+1 < 7) && (origine[x+1][y+1] == autre)) {
		autrePion = 0;
		possible = 1;
		i = x+2;
		j = y+2;
		while (!autrePion && possible && i <= 7 && j <= 7) {
			if (origine[i][j] == VIDE) {
				possible = 0;
			}
			else if (origine[i][j] == joueur) {
				autrePion = 1;
			}
			else {
				i++;
				j++;
			}
		}
		if (autrePion) {
			i--;
			j--;
			while (i > x && j > y) {
				res[i][j] = joueur;
				i--;
				j--;
			}
		}
	}
	return res;
}

void afficheTab(int ** tab) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			printf("%d ",tab[i][j]);
		}
		printf("\n");
	}
}

void freeTab(int ** tab){
	for(int i = 0; i<8; i++){
		free(tab[i]);
	}
	free(tab);
}

int ** creerTabJeu(int choix) {
	int ** tabJeu = creerTab();

	char fileName[25];
	
	if (choix == 0) {
		// printf("Vous avez choisi la génération classique\n");
		for(int i=0; i<8;i++){
			for(int j=0;j<8;j++){
				if(i==j && (i==3||i==4)){
					tabJeu[j][i]=BLANC;
				}
				else if((i==4 && j==3) || (i==3 && j==4)){
					tabJeu[j][i]=NOIR;
				}
				else{
					tabJeu[j][i]=VIDE;
				}

			}
		}
	}
	else if (choix > 0 && choix <= 5) {
		// printf("Vous avez choisi la génération à partir du fichier n°%d\n",choix);
		sprintf(fileName,"../Maps/cas_%d.txt",choix);
		FILE * f =fopen(fileName, "r");
		if (f == NULL) {
			perror(fileName);
			exit(EXIT_FAILURE);
		}
		else {
			int size = (8+1)*8;
			char buffer[size];
			if (fread(buffer, sizeof(char), size, f) > 0) {
				int i = 0, j = 0, k = 0;
				int fin_Ligne = 0;
				while ( i < 8 && j <= 8 && k < size) {
					if (! fin_Ligne) {
						switch (buffer[k]) {
							case 'V' :
							tabJeu[i][j] = VIDE;
							j++;
							k++;
							break;
							case 'B' :
							tabJeu[i][j] = BLANC;
							j++;
							k++;
							break;
							case 'N' :
							tabJeu[i][j] = NOIR;
							j++;
							k++;
							break;
							default :
							printf("Présence de caractère anormal\n");
							exit(EXIT_FAILURE); 
							break;
						}
					}
					else {
						switch (buffer[k]) {
							case '\n' :
							i++;
							j = 0;
							k++;
							fin_Ligne = 0;
							break;
							default :
							printf("Présence de caractère anormal\n");
							exit(EXIT_FAILURE); 
							break;
						}
					}
					if (j == 8) {
						fin_Ligne = 1;
					}
				}
			}
			else {
				fclose(f);
				perror("Erreur lors de la lecture du fichier");
				exit(EXIT_FAILURE);
			}
			fclose(f);
		}
	}
	else {
		// printf("Votre choix n'est pas valide\n");
	}
	return tabJeu;
}

void copieTab (int ** origine, int ** arrivee) {
	for (int i = 0; i < 8; i++) {
		// memcpy(arrivee,origine,8*sizeof(int));
		for (int j = 0; j < 8; j++) {
			// printf("%d ", arrivee[j][i]);
			// if (origine[i][j] != arrivee[j][i]) {
			// 	exit(EXIT_FAILURE);
			// }
			arrivee[i][j] = origine[i][j];
		}
		// printf("\n");
	}
}

int ** creerTab () {
	int ** tab = malloc(8*sizeof(int *));
	if (tab != NULL) {
		for (int i=0; i < 8; i++) {
			tab[i] = malloc(8*sizeof(int));
			if (tab[i] == NULL) {
				printf("Erreur lors de la génération du tableau\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	else {
		printf("Erreur lors de la génération du tableau\n");
		exit(EXIT_FAILURE);
	}
	return tab;
}

int nbPoints (int ** tab) {
	int nb_blanc = 0, nb_noir = 0;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			switch (tab[i][j]) {
				case BLANC :
				nb_blanc++;
				break;
				case NOIR :
				nb_noir++;
				break;
				default :
				break;
			}
		}
	}
	int res = nb_blanc - nb_noir;
	return res;
}