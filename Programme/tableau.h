/**
 * \file tableau.h
 * \brief Gestion des tableaux
 *
 * Fonctions de gestion des tableaux en particulier des tabliers de jeu.
 */

/** 
 * \fn int ** creerTabJeu (int choix)
 * \brief Fonction de création de la situation de départ d'une partie.
 *
 * Cette fonction permet de choisir comment remplir le tableau, on peut choisir entre la situation de départ classique pour une partie d'othello et des situations définies par des fichiers .txt. Ces fichiers doivent faire 8 lignes de 8 caractères (pouvant être B pour BLANC, N pour NOIR et V pour VIDE, voir \ref Pions) suivis impérativement d'un retour à la ligne (dernière ligne comprise).
 *
 * \return Le tableau de la situation de départ de toute partie.
 */
int ** creerTabJeu (int choix);

/**
 * \fn int ** genTableau (int ** origine, int x, int y, int joueur)
 * \brief Fonction de création d'un nouveau tableau suite à un coup joué.
 * 
 * \param origine Le tableau avant que le coup ne soit joué.
 * \param x La coordonée sur x du coup joué.
 * \param y La coordonée sur y du coup joué.
 * \param joueur Le joueur actuel, peut être BLANC ou NOIR (\ref Pions).
 * \return Le tableau de la situation modifiée par le coup joué.
 */
int **  genTableau (int ** origine,int x, int y, int joueur);

/**
 * \fn void afficheTab (int ** tab)
 * \brief Affiche dans le terminal un tableau 2D de 8x8.
 * 
 * \param tab Tableau à afficher.
 */
void afficheTab (int ** tab);

/**
 * \fn void freeTab (int ** tab)
 * \brief Libère l'espace mémoire alloué à un tableau 2D de 8x8.
 *
 * \param tab Tableau à libèrer.
 */
void freeTab (int ** tab); 

/**
 * \fn void copieTab (int ** origine, int ** arrivee)
 * \brief Copie le contenu d'un tableau 2D dans un autre tableau 2D (8x8).
 *
 * \param origine Tableau à copier.
 * \param arrivee Tableau dans lequel copier.
 */
void copieTab (int ** origine, int ** arrivee);

/**
 * \fn int ** creerTab ()
 * \brief Création d'un tableau 2D de 8x8.
 *
 * \return L'adresse du tableau créé.
 */
int ** creerTab ();

/**
 * \fn int nbPoints (int ** tab)
 * \brief Cette fonction permet de savoir si blanc gagne, perd ou s'il y a égalité.
 *
 * \param tab Tablier à partir duquel calculer le nbPoints
 * \return le nombre de points noirs, en négatif, si blanc perd, le nombre de points blancs si blanc gagne et 0 s'il y a égalité 
 */
int nbPoints (int ** tab);