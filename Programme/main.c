#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <SDL2/SDL.h>
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "arbre.h"
#include "tableau.h"
#include "affichage.h"
#include "constantes.h"
#include "minmax.h"

int main (int argc, char ** argv) {
    srand(time(0));

    int continuer = 1;

    SDL_Window * window;
    SDL_Renderer * ecran;

    if (  SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        exit(EXIT_FAILURE);
    }
    else {
        if (SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_MAXIMIZED, &window, &ecran) < 0) {
            printf("Erreur d'initialisation de la fenêtre et du renderer : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
        else {
            SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); 
            SDL_RenderSetLogicalSize(ecran, SCREEN_WIDTH, SCREEN_HEIGHT);
            SDL_SetRenderDrawColor(ecran, 0, 0, 0, 255);
            SDL_RenderClear(ecran);
            SDL_RenderPresent(ecran);
            SDL_UpdateWindowSurface(window);

            SDL_Delay( 1000 );
        }
    }

    TTF_Font * police = NULL;
    if(TTF_Init() < 0) {
        printf("Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    police = TTF_OpenFont("../Polices/angelina.ttf",60);

    if (police == NULL) {
        printf("TTF_OpenFont : %s\n", TTF_GetError());
    }

    if(!(IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG)) {
        printf("Erreur d'initialisation de IMG_Init : %s\n", IMG_GetError());
    }

    int profondeurB = 0;
    int profondeurN = 0;

    while (continuer) {
        int choixJ = menuChoixJoueur(window, ecran, police);
        int (*pchoix1)(int, int**) = NULL;
        int (*pchoix2)(int, int**) = NULL;
        switch (choixJ) {
            case 1 :
            pchoix1 = &choixAlea;
            profondeurB = menuProfondeur(window, ecran, police, BLANC);
            if (profondeurB > 0) {
                pchoix2 = &choixAlea;
                profondeurN = menuProfondeur(window, ecran, police, NOIR);
                if (profondeurN == -1) {
                    continuer = 0;
                }
            }
            else {
                continuer = 0;
            }
            break;
            case 2 :
            pchoix1 = &choixJoueur;
            profondeurB = menuProfondeur(window, ecran, police, BLANC);
            pchoix2 = &choixAlea;
            profondeurN = menuProfondeur(window, ecran, police, NOIR);
            break;
            case 3 :
            pchoix1 = &choixJoueur;
            profondeurB = menuProfondeur(window, ecran, police, BLANC);
            pchoix2 = &choixJoueur;
            profondeurN = menuProfondeur(window, ecran, police, NOIR);
            break;
            case -1:
            continuer = 0;
            break;
            default :
            break;
        }

        if (continuer) {
            int ** tabPos = creerTab();
            int choixP = menuPrincipal(window, ecran, police);
            if (choixP >= 0) {
                int ** tabDeBase = creerTabJeu(choixP);
                Maillon coup1;
                coup1.next_ = NULL;
                coup1.prec_ = NULL;
                coup1.tab = tabDeBase;
                coup1.aucunCoup = 0;
                coup1.coupsGen = NULL;
                int score = partie(window, ecran, &coup1, tabPos, pchoix1, pchoix2, profondeurB, profondeurN);
                int winner = VIDE;
                int fin = -1;
                if (score <= 64 && score >= -64) {
                    if (score < 0) {
                        winner = NOIR;
                        score = (-1)*score;
                    }
                    else if (score > 0) {
                        winner = BLANC;
                    }
                    else {
                    }
                    fin = menuFin(window, ecran, police, winner, score);
                }
                else {
                    fin = menuPause(window, ecran, police);
                }
                if (fin == -1) {
                    continuer = 0;
                }
                else if (fin == 1) {
                    continuer = 1;
                }
                freeTab(tabDeBase);
                freeListe(coup1.coupsGen);
            }
            else {
                continuer = 0;
            }
            freeTab(tabPos);
        }
    }




    TTF_CloseFont(police);
    SDL_DestroyRenderer(ecran);
    SDL_DestroyWindow(window);
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
    return EXIT_SUCCESS;
}
