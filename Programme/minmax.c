#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "SDL2/SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "arbre.h"
#include "tableau.h"
#include "constantes.h"
#include "affichage.h"
#include "minmax.h"

int bord (Maillon * maillon) {
	int res = 0;
	if (maillon->coordx == 0 || maillon->coordx == nbCase - 1) {
		res++;
	}
	else {
	}
	if (maillon->coordy == 0 || maillon->coordy == nbCase - 1) {
		res++;
	}
	else {
	}
	return res; 
}

void noteCoups(Maillon * maillon, int nbTour, int ** tabPos){
	int nbPionsJ1 = 0;
	int nbPionsJ2 = 0;
	int nbCoupsPosJ1 = 0;
	int nbCoupsPosJ2 = 0;
	int j1 = nbTour%2;
	int j2 = (nbTour+1)%2;
	//int nbPionsPosesJ1 = 0;
	//int nbPionsPosesJ2 = 0;
	Maillon * current = maillon; 


	//récupération du nombre de pions imprenables de J1 
	for(int x = 0; x < nbCase; x++){

		for(int y = 0; y < nbCase; y++){

			if(maillon->tab[x][y]==j1){
				//nbPionsPosesJ1++;
				int ligneSafe = 0;
				if((x+1) < nbCase && (y+1) < nbCase && maillon->tab[x+1][y+1]==j1) {
					int caseDanger = 0;
					int i=x+1;
					int j=y+1;
					do{
						i++;
						j++;
					}
					while((i < nbCase && j < nbCase) && maillon->tab[i][j] == j1);
					if(i < nbCase && j < nbCase && maillon->tab[i][j]!=j1){
						caseDanger++;	
					}
					if((x-1) >= 0 && (y-1) >= 0 && maillon->tab[x-1][y-1]==j1){
						int a=x-1;
						int b=y-1;
						do{
							a--;
							b--;
						}
						while((a>=0 && b>=0) && maillon->tab[a][b] == j1);
						if(a >= 0 && b >= 0 && maillon->tab[a][b]!=j1){
							caseDanger++;
						}
					}
					if(caseDanger<2){
						ligneSafe ++;
					}
				}
				if((x+1) < nbCase && maillon->tab[x+1][y]==j1) {
					int caseDanger = 0;
					int i=x+1;
					do{
						i++;
					}
					while((i < nbCase ) && maillon->tab[i][y] == j1);
					if(i < nbCase && maillon->tab[i][y]!=j1){
						caseDanger++;	
					}
					if((x-1) >= 0 && maillon->tab[x-1][y]==j1){
						int a=x-1;
						do{
							a--;
						}
						while((a>=0) && maillon->tab[a][y] == j1);
						if(a >= 0 && maillon->tab[a][y]!=j1){
							caseDanger++;
						}
					}
					if(caseDanger<2){
						ligneSafe ++;
					}
				}
				if((x+1) < nbCase && (y-1) >=0 && maillon->tab[x+1][y-1]==j1) {
					int caseDanger = 0;
					int i=x+1;
					int j=y-1;
					do{
						i++;
						j--;
					}
					while((i < nbCase && j >= 0) && maillon->tab[i][j] == j1);
					if(i < nbCase && j >= 0 && maillon->tab[i][j]!=j1){
						caseDanger++;	
					}
					if((x-1) >= 0 && (y+1) < nbCase && maillon->tab[x-1][y+1]==j1){
						int a=x-1;
						int b=y+1;
						do{
							a--;
							b++;
						}
						while((a>=0 && b<nbCase) && maillon->tab[a][b] == j1);
						if(a >= 0 && b <nbCase && maillon->tab[a][b]!=j1){
							caseDanger++;
						}
					}
					if(caseDanger<2){
						ligneSafe ++;
					}
				}
				if((y+1) < nbCase && maillon->tab[x][y+1]==j1) {
					int caseDanger = 0;
					int j=y+1;
					do{
						j++;
					}
					while((j < nbCase) && maillon->tab[x][j] == j1);
					if(j < nbCase && maillon->tab[x][j]!=j1){
						caseDanger++;	
					}
					if((y-1) >= 0 && maillon->tab[x][y-1]==j1){
						int b=y-1;
						do{
							b--;
						}
						while((b>=0) && maillon->tab[x][j] == j1);
						if(j >= 0 && maillon->tab[x][j]!=j1){
							caseDanger++;
						}
					}
					if(caseDanger<2){
						ligneSafe ++;
					}
				}
				if(ligneSafe == 4){
					nbPionsJ1++;
				}					
			}
		}
	}

	Maillon * max = NULL;
	Maillon * min = NULL;
	//création des coups possible de J2 a partir de tout les possible de J1

	if(maillon->coupsGen != NULL && longListe(maillon->coupsGen) > 0){
		current = maillon->coupsGen->head_;
		max = current;
		min = current;										
		for(int k = 0; k < longListe(maillon->coupsGen); k++){
		//récupération nombre de coup possible pour J1 a partir du maillon
			trouverCoups(current, nbTour+1, tabPos);

		//récupération du nombre de pions imprenables de J2
			nbCoupsPosJ2 = current->coupsGen->nb_elements_;
			for(int x = 0; x < nbCase; x++){
				for(int y = 0; y < nbCase; y++){
					if(current->tab[x][y]==j2){
					//nbPionsPosesJ2++;
						int ligneSafe = 0;
						if((x+1) < nbCase && (y+1) < nbCase && current->tab[x+1][y+1]==j2) {
							int caseDanger = 0;
							int i=x+1;
							int j=y+1;
							do{
								i++;
								j++;
							}
							while((i < nbCase && j < nbCase) && current->tab[i][j] == j2);
							if(i < nbCase && j < nbCase && current->tab[i][j]!=j2){
								caseDanger++;	
							}
							if((x-1) >= 0 && (y-1) >= 0 && current->tab[x-1][y-1]==j2){
								int a=x-1;
								int b=y-1;
								do{
									a--;
									b--;
								}
								while((a>=0 && b>=0) && current->tab[a][b] == j2);
								if(a >= 0 && b >= 0 && current->tab[a][b]!=j2){
									caseDanger++;
								}
							}
							if(caseDanger<2){
								ligneSafe ++;
							}
						}
						if((x+1) < nbCase && current->tab[x+1][y]==j2) {
							int caseDanger = 0;
							int i=x+1;
							do{
								i++;
							}
							while((i < nbCase ) && current->tab[i][y] == j2);
							if(i < nbCase && current->tab[i][y]!=j2){
								caseDanger++;	
							}
							if((x-1) >= 0 && current->tab[x-1][y]==j2){
								int a=x-1;
								do{
									a--;
								}
								while((a>=0) && current->tab[a][y] == j2);
								if(a >= 0 && current->tab[a][y]!=j2){
									caseDanger++;
								}
							}
							if(caseDanger<2){
								ligneSafe ++;
							}
						}
						if((x+1) < nbCase && (y-1) >=0 && current->tab[x+1][y-1]==j2) {
							int caseDanger = 0;
							int i=x+1;
							int j=y-1;
							do{
								i++;
								j--;
							}
							while((i < nbCase && j >= 0) && current->tab[i][j] == j2);
							if(i < nbCase && j >= 0 && current->tab[i][j]!=j2){
								caseDanger++;	
							}
							if((x-1) >= 0 && (y+1) < nbCase && current->tab[x-1][y+1]==j2){
								int a=x-1;
								int b=y+1;
								do{
									a--;
									b++;
								}
								while((a>=0 && b<nbCase) && current->tab[a][b] == j2);
								if(a >= 0 && b <nbCase && current->tab[a][b]!=j2){
									caseDanger++;
								}
							}
							if(caseDanger<2){
								ligneSafe ++;
							}
						}
						if((y+1) < nbCase && current->tab[x][y+1]==j2) {
							int caseDanger = 0;
							int j=y+1;
							do{
								j++;
							}
							while((j < nbCase) && current->tab[x][j] == j2);
							if(j < nbCase && current->tab[x][j]!=j2){
								caseDanger++;	
							}
							if((y-1) >= 0 && current->tab[x][y-1]==j2){
								int b=y-1;
								do{
									b--;
								}
								while((b>=0) && current->tab[x][j] == j2);
								if(j >= 0 && current->tab[x][j]!=j2){
									caseDanger++;
								}
							}
							if(caseDanger<2){
								ligneSafe ++;
							}
						}
						if(ligneSafe == 4){
							nbPionsJ2++;
						}					
					}
				}
			}
			for (int l = 0; l < longListe(current->coupsGen); l++){
			//récupération nombre de coup possible pour J1 a partir du maillon
				nbCoupsPosJ1 = maillon->coupsGen->nb_elements_;
			//calcul de la note pour chaque coup
				current->poids = ((-1)*(j1 - j2)) * (1*(nbCoupsPosJ1 - nbCoupsPosJ2) + 1*(nbPionsJ1 - nbPionsJ2));	
			}

			if (current->poids > max->poids) {
				max = current;
			}
			else if (current->poids == max->poids) {
				if (bord(current) > bord(max)) {
					max = current;
				}
				else if (max == min && bord(current) == bord(max)){
					max = current;
				}
				else {
				}
			}
			else {
			}

			if (current->poids < min->poids) {
				min = current;
			}
			else if (current->poids == min->poids) {
				if (bord(current) > bord(min)) {
					min = current;
				}
				else if (max == min && bord(current) == bord(min)) {
					min = current;
				}
				else {
				}
			}
			else {
			}

			current = current->next_;

		}

		if (max != min) {
			addHead(maillon->coupsGen, removeMaillon(maillon->coupsGen, max));
			addTail(maillon->coupsGen, removeMaillon(maillon->coupsGen, min));
		}
		else {
			if (nbTour+1%2 == 0) {
				addHead(maillon->coupsGen, removeMaillon(maillon->coupsGen, max));
			}
			else {
				addTail(maillon->coupsGen, removeMaillon(maillon->coupsGen, min));
			}
		}
	}
	else{
	}
}

int choixMinMax(Maillon * maillon, int nbTour, int profondeur, int ** tabPos){
	int choice = -1;
	int x = -1, y = -1;
	SDL_Event event;
	
	minMaxDown(maillon, nbTour, 0, profondeur, tabPos);

	for (int i = 0; i < 200; i++) {
		SDL_PollEvent(&event);
	}

	if (nbTour%2 == BLANC) {
		choice = valMin(maillon, 0, profondeur);

	}
	else {
		choice = valMax(maillon, 0, profondeur);
	}

	for (int i = 0; i < 200; i++) {
		SDL_PollEvent(&event);
	}

	switch (event.type) {
		case SDL_QUIT :
		choice = -1;
		break;
		case SDL_MOUSEBUTTONDOWN :
		SDL_GetMouseState(&x,&y);
		if (x > SCREEN_WIDTH*3/4) {
			if (y > SCREEN_HEIGHT/2) {
				if (y < SCREEN_HEIGHT*3/4) {
					choice = -1;
				}
			}
		}
	}

	return choice;
}

void remplissageMinMax (Maillon * maillon, int nbTour, int profondeur, int ** tabPos) {
	int choice = -1;

	minMaxDown(maillon, nbTour, 0, profondeur, tabPos);

	if (nbTour%2 == BLANC) {
		choice = valMin(maillon, 0, profondeur);
	}
	else {
		choice = valMax(maillon, 0, profondeur);
	}
	choice++;
}

void minMaxDown (Maillon * maillon, int nbTour, int profondeurAct, int profondeurMax, int ** tabPos) {
	if (profondeurAct == profondeurMax-1) {
		trouverCoups(maillon, nbTour, tabPos);
		noteCoups(maillon, nbTour, tabPos);
	}
	else {
		trouverCoups(maillon, nbTour, tabPos);
		if (maillon->coupsGen != NULL && longListe(maillon->coupsGen) > 0) {
			Maillon * current = maillon->coupsGen->head_;
			for (int i = 0; i < longListe(maillon->coupsGen); i++) {
				minMaxDown(current, nbTour+1, profondeurAct+1, profondeurMax, tabPos);
				current = current->next_;
			}
		}
		else {
			noteCoups(maillon->origine_, nbTour-1, tabPos);
		}
	}
}

int valMax(Maillon * maillon, int profondeur, int profondeurMax){
	srand(time(NULL));
	int res = 0;
	int choice = -1;
	Maillon * max = NULL;
	int maxBord = 0;
	Liste * l = maillon->coupsGen;
	if(l != NULL && longListe(l) > 0){
		if (profondeur < profondeurMax-1) {  // On veut remonter dans ce maillon la note max des fils donc il faut être < profondeur-1
			Maillon * current = l->head_;
			for (int i = 0; i < longListe(l); i++) {
				current->poids = valMin(current, profondeur+1, profondeurMax);
				current = current->next_;
			}
		}
		Maillon * current = l->head_;
		for(int i = 0; i < l->nb_elements_; i++){
			if(max == NULL || current->poids > max->poids){
				max = current;
				maxBord = bord(max);
				choice = i;
			}
			else if (current->poids == max->poids){
				if (bord(current) > maxBord) {
					max = current;
					maxBord = bord(max);
					choice = i;
				}
				else if (bord(current) == bord(max)){
					if(rand()%2 == 1){
						max = current;
						maxBord = bord(max);
						choice = i;
					}
					else {
					}
				}
				else {
				}
			}
			else {
			} 
			current = current->next_;
		}
		if (profondeur == 0) {
			res = choice;
		}
		else {
			res = max->poids;
		}
	}
	else{
		res = maillon->poids;
	}
	return res;
}

int valMin(Maillon * maillon, int profondeur, int profondeurMax){
	srand(time(NULL));
	int res = 0;
	int choice = -1;
	Maillon * min = NULL;
	int minBord = 0;
	Liste * l = maillon->coupsGen;
	if(l != NULL && longListe(l) > 0){
		if (profondeur < profondeurMax-1) {  // On veut remonter dans ce maillon la note min des fils donc il faut être < profondeur-1
			Maillon * current = l->head_;
			for (int i = 0; i < longListe(l); i++) {
				current->poids = valMax(current, profondeur+1, profondeurMax);
				current = current->next_;
			}
		}
		Maillon * current = l->head_;
		for(int i = 0; i < l->nb_elements_; i++){
			if(min == NULL){
				min = current;
				minBord = bord(min);
				choice = i;
			}
			else if (current->poids < min->poids) {
				min = current;
				minBord = bord(min);
				choice = i;
			}
			else if (current->poids == min->poids){
				if (bord(current) > minBord) {
					min = current;
					minBord = bord(min);
					choice = i;
				}
				else if (bord(current) == bord(min)){
					if(rand()%2 == 1){
						min = current;
						minBord = bord(min);
						choice = i;
					}
					else {
					}
				}
				else {
				}
			}
			else {
			} 
			current = current->next_;
		}
		if (profondeur == 0) {
			res = choice;
		}
		else {
			res = min->poids;
		}
	}
	else{
		res = maillon->poids;
	}
	return res;
}

// int alphaBeta(Maillon * maillon, int nbTour, int profondeur, int ** tabPos){
// 	int choice = 0;
// 	if(maillon->coupsGen == NULL || longListe(maillon->coupsGen)==0 || profondeur == 0){
// 		if(nbTour%2==0){
// 			choice = valMax(noteCoups(maillon, profondeur, tabPos));
// 		}
// 		else if(nbTour%2 == 1){
// 			choice = valMin(noteCoups(maillon, profondeur, tabPos));
// 		}		
// 	}
// 	else{
		
// 		Maillon * current = maillon->coupsGen->head_;
// 		int i = current->prec_->poids;
// 		int j = -100;
// 		while(current->next_ != NULL){
// 			j = Max(j, alphaBeta(current, nbTour, profondeur, tabPos));
// 			if((-1)*j <= i){
// 				choice = -j;
// 			}
// 			current = current->next_;
// 		}
// 		int alpha = -100;
// 		int beta = 100;
// 		Maillon * current = maillon->coupsGen->head_;
// 		if(nbTour%2 == 1){
// 			int v = 100;
// 			while(current->next_ != NULL){
// 				v = Min(v, alphaBeta(current, nbTour, profondeur, tabPos));
// 				if(alpha >= v){
// 					choice = v;
// 				}
// 				beta = Min(beta, v);
// 			}
// 			current = current->next_;
// 		}
// 		else if(nbTour%2==0){
// 			int v = -100;
// 			while(current->next_ != NULL){
// 				v = Max(v, alphaBeta(current, nbTour, profondeur, tabPos));
// 				if(v >= beta){
// 					choice = v;
// 				}
// 				alpha = Max(alpha, v);
// 			}
// 			current = current->next_;
// 		}
// 	}
// 	return choice;
// }

int Max(int a, int b){
	int max = 0;
	if(a>=b){
		max = a;
	}
	else{
		max = b;
	}
	return max;
}
int Min(int a, int b){
	int min = 0;
	if(a<=b){
		min = a;
	}
	else{
		min = b;
	}
	return min;
}