	#include <stdio.h>
	#include <stdlib.h>
	#include "SDL2/SDL.h"
	#include "SDL_ttf.h"
	#include "SDL_image.h"
	#include "arbre.h"
	#include "tableau.h"
	#include "constantes.h"
	#include "affichage.h"
	#include "minmax.h"

Liste * creerListe() {
	Liste * liste = malloc(sizeof(Liste));
	if (liste == NULL) {
		printf("creerListe : problème lors de la création de la liste\n");
		exit(EXIT_FAILURE);
	}
	liste->head_ = liste->tail_;
	liste->nb_elements_ = 0;
	return liste;
}

int longListe(Liste * liste) {
	return liste->nb_elements_;
}

void addTail(Liste * liste, Maillon * maillon) {
	if(liste->nb_elements_ == 0) {
		liste->head_  = liste->tail_ = maillon;
	}
	else {
		maillon->prec_ = liste->tail_;
		maillon->next_ = NULL;
		liste->tail_->next_ = maillon;
		liste->tail_ = maillon;
	}
	liste->nb_elements_++;
}

void addHead(Liste * liste, Maillon * maillon) {
	if(liste->nb_elements_ == 0) {
		liste->head_ = liste->tail_ = maillon;
	}
	else {
		maillon->prec_ = NULL;
		maillon->next_ = liste->head_;
		liste->head_->prec_ = maillon;
		liste->head_ = maillon;
	}
	liste->nb_elements_++;
}

Maillon * removeMaillon (Liste * liste, Maillon * maillon) {

	if (maillon != NULL && longListe(liste) > 0) {
		// lectureListe(liste);
		// printf("Maillon :\nx: %d  y: %d\n", maillon->coordx, maillon->coordy);
		if (maillon->prec_ != NULL) {
			// printf("1.1\n");
			maillon->prec_->next_ = maillon->next_;
			// printf("1.2\n");
		}
		if (maillon->next_ != NULL) {
			// printf("2.1\n");
			maillon->next_->prec_ = maillon->prec_;
			// printf("2.2\n");
		}
		if (liste->head_ == maillon) {
			// printf("3.1\n");
			liste->head_ = maillon->next_;
			// printf("3.2\n");
		}
		if (liste->tail_ == maillon) {
			// printf("4.1\n");
			liste->tail_ = maillon->prec_;
			// printf("4.2\n");
		}
		// printf("nb_elements_ : %d\n", liste->nb_elements_);
		liste->nb_elements_--;
		// printf("nb_elements_ 2: %d\n", liste->nb_elements_);
		maillon->next_ = NULL;
		maillon->prec_ = NULL;
		// if (longListe(liste) > 0) {
		// 	lectureListe(liste);
		// }
	}
	return maillon;
}

	// int valHead(Liste * liste) {
	// 	return liste->head_->aucunCoup;
	// }

	// int valTail(Liste * liste) {
	// 	return liste->tail_->aucunCoup;
	// }

void lectureListe(Liste * liste) {
	int continuer = liste->nb_elements_;
	Maillon * current = liste->head_;
	// printf("Head : \nx: %d  y: %d\n", liste->head_->coordx, liste->head_->coordy);
	// printf("Tail : \nx: %d  y: %d\n", liste->tail_->coordx, liste->tail_->coordy);
	while(continuer) {
		printf("Continuer : %d\n",continuer);
		printf("Poids: %d\n", current->poids);
		current = current->next_;
		continuer--;
	}
}

void genListe (int nbCoups, int nbTour, int joueur, Maillon * origine,int ** tabPos) {
	int i = 0;
	int j = 0;
	if (nbCoups == 0 && origine->aucunCoup == 0) {
		Maillon * coup_actuel = malloc(sizeof(Maillon));
		if(coup_actuel == NULL) {
			printf("genListe : problème de création d'un maillon\n");
			exit(EXIT_FAILURE);
		}
		coup_actuel->poids = 0;
		coup_actuel->next_ = NULL;
		coup_actuel->prec_ = NULL;
		coup_actuel->origine_ = origine;
		coup_actuel->tab = creerTab();
		copieTab(origine->tab,coup_actuel->tab);
		coup_actuel->aucunCoup = 1;
		coup_actuel->coupsGen = NULL;
		coup_actuel->coordx = -1;
		coup_actuel->coordy = -1;
		addHead(origine->coupsGen,coup_actuel);
	}
	else { 
	}
	while (nbCoups > 0 && i < 8 && j < 8) {
		if (tabPos[i][j] > 0) {
			Maillon * coup_actuel = malloc(sizeof(Maillon));
			if(coup_actuel == NULL) {
				printf("genListe : problème de création d'un maillon\n");
				exit(EXIT_FAILURE);
			}
			coup_actuel->poids = 0;
			coup_actuel->next_ = NULL;
			coup_actuel->prec_ = NULL;
			coup_actuel->origine_ = origine;
			coup_actuel->tab = genTableau(origine->tab,i,j,joueur);
			coup_actuel->aucunCoup = 0;
			coup_actuel->coupsGen = NULL;
			coup_actuel->coordx = i;
			coup_actuel->coordy = j;
			addHead(origine->coupsGen,coup_actuel);
			nbCoups--;
		}
		else {
		}
		i++;
		if (i == 8) {
			j++;
			i = 0;
		}
		else {
		}
	}
}

void freeListe (Liste * liste) {
	int continuer = liste->nb_elements_;
	Maillon * current = liste->head_;
	while (continuer) {
		freeTab(current->tab);
		if (current->coupsGen != NULL) {
			freeListe(current->coupsGen);
		}
		if (current->next_ != NULL) {
			current = current->next_;
			free(current->prec_);
		}
		else {
			free(current);
		}
		continuer--;
	}
	free(liste);
} 

void trouverCoups(Maillon * maillon, int nTour, int ** tabPos){
	if (maillon->coupsGen == NULL) {
		for(int i=0; i<8; i++){
			for(int j=0; j<8; j++){
				tabPos[i][j]=0;
			}
		}
		int joueur = nTour%2; 
		int autre = (nTour+1)%2;
		int x, y, coups=0, nbCoups=0;
		for(x=0; x<8; x++){
			for(y=0; y<8; y++){
				if(maillon->tab[x][y]==VIDE){
					if((x+1) < 8 && (y+1) < 8 && maillon->tab[x+1][y+1]==autre) {
						int i=x+1;
						int j=y+1;
						do{
							i++;
							j++;
						}
						while((i<8 && j<8) && maillon->tab[i][j] == autre);
						if(i < 8 && j < 8 && maillon->tab[i][j]==joueur){
							coups++;
						}
					}
					if((x+1) < 8 && maillon->tab[x+1][y]==autre){
						int i=x+1;
						do{
							i++;
						}
						while(i<8 && maillon->tab[i][y] == autre);
						if(i < 8 && maillon->tab[i][y]==joueur){
							coups++;
						}
					}
					if((x+1) < 8 && (y-1) >= 0 && maillon->tab[x+1][y-1]==autre){
						int i=x+1;
						int j=y-1;
						do{
							i++;
							j--;
						}
						while((i<8 && j>=0) && maillon->tab[i][j] == autre);
						if(i < 8 && j >= 0 && maillon->tab[i][j]==joueur){
							coups++;
						}
					}
					if((y+1) < 8 && maillon->tab[x][y+1]==autre){
						int j=y+1;
						do{
							j++;
						}
						while(j<8 && maillon->tab[x][j] == autre);
						if(j < 8 && maillon->tab[x][j]==joueur){
							coups++;
						}
					}
					if((y-1) >= 0 && maillon->tab[x][y-1]==autre){
						int j=y-1;
						do{
							j--;
						}
						while(j >= 0 && maillon->tab[x][j] == autre);
						if(j >= 0 && maillon->tab[x][j]==joueur){
							coups++;
						}
					}
					if((x-1) >= 0 && (y+1) < 8 && maillon->tab[x-1][y+1]==autre){
						int i=x-1;
						int j=y+1;
						do{
							i--;
							j++;
						}
						while((i>=0 && j<8) && maillon->tab[i][j] == autre);
						if( i >= 0 && j < 8 && maillon->tab[i][j]==joueur){
							coups++;
						}
					}
					if((x-1) >= 0 && maillon->tab[x-1][y]==autre){
						int i=x-1;
						do{
							i--;
						}
						while(i >= 0 && maillon->tab[i][y] == autre);
						if(i >= 0 && maillon->tab[i][y]==joueur){
							coups++;
						}
					}
					if((x-1) >= 0 && (y-1) >= 0 && maillon->tab[x-1][y-1]==autre){
						int i=x-1;
						int j=y-1;
						do{
							i--;
							j--;
						}
						while((i>=0 && j>=0) && maillon->tab[i][j] == autre);
						if(i >= 0 && j >= 0 && maillon->tab[i][j]==joueur){
							coups++;
						}
					}
					if(coups!=0){
						nbCoups++;
						coups=0;
						tabPos[x][y]=nbCoups;
					}
				}
			}
		}

		// Génération de la liste

		Liste * liste = creerListe();
		maillon->coupsGen = liste;
		if (maillon->coupsGen == NULL) {
			printf("Error list generation\n");
		}
		genListe(nbCoups, nTour, joueur,maillon, tabPos);
	}
}

int partie(SDL_Window * window, SDL_Renderer * screen, Maillon * current, int ** tabPos, int (*pchoix1)(int, int**), int (*pchoix2)(int, int**), int profondeurB, int profondeurN){
	int continuer = 1;
	int nbTour = 1;
	int res = 65;
	SDL_Surface * plateau = IMG_Load("../ressources_Images/plateau2.png");
	SDL_Surface * pionBlanc = IMG_Load("../ressources_Images/pionBlanc2.png");
	SDL_Surface * pionNoir = IMG_Load("../ressources_Images/pionNoir2.png");
	SDL_Surface * coupJouable = IMG_Load("../ressources_Images/coupJouable.png");
	if(plateau == NULL){
		printf( "Unable to load image : ../ressources_Images/plateau2.png ! SDL_image Error: %s\n", SDL_GetError() );
		exit(EXIT_FAILURE);
	}
	if(pionBlanc == NULL){
		printf( "Unable to load image : ../ressources_Images/pionBlanc2.png ! SDL_image Error: %s\n", SDL_GetError() );
		exit(EXIT_FAILURE);
	}
	if(pionNoir == NULL){
		printf( "Unable to load image : ../ressources_Images/pionNoir2.png ! SDL_image Error: %s\n", SDL_GetError() );
		exit(EXIT_FAILURE);
	}
	if(coupJouable == NULL) {
		printf( "Unable to load image : ../ressources_Images/coupJouable.png ! SDL_image Error: %s\n", SDL_GetError() );
		exit(EXIT_FAILURE);
	}
	int ** tabTemp = creerTab();
	int ** tabPoids = creerTab();

	while(continuer){
		if (current != NULL) {
			if (nbTour%2 == BLANC) {
				remplissageMinMax(current, nbTour, profondeurB, tabPos);
			}
			else {
				remplissageMinMax(current, nbTour, profondeurN, tabPos);
			}
			
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					tabTemp[i][j] = 0;
					tabPoids[i][j] = 0;
				}
			}
			if((current->coupsGen == NULL) || (longListe(current->coupsGen) == 0)){
				res = nbPoints(current->tab);
				continuer = 0;
			}
			else{
				int continuer2 = longListe(current->coupsGen);
				Maillon * current2 = current->coupsGen->head_;
				int coupTemp = 1;
				while(continuer2){
					if(current2->coordx >= 0 && current2->coordy >= 0){
						tabTemp[current2->coordx][current2->coordy] = coupTemp;
						tabPoids[current2->coordx][current2->coordy] = current2->poids;
						coupTemp++;
					}
					continuer2--;
					if (current2->next_ != NULL) {
						current2 = current2->next_;
					}
					else {
						continuer2 = 0;
					}
				}
				affichePlateau(window, screen, plateau, pionBlanc, pionNoir, coupJouable, current->tab, tabTemp, tabPoids, nbTour);
				if(nbTour%2 == 0){
					current = coup(current, tabTemp, nbTour, pchoix1, profondeurB);
				}
				else if(nbTour%2 == 1){
					current = coup(current, tabTemp, nbTour, pchoix2, profondeurN);
				}
			}
			SDL_Delay(500);
			nbTour++;
		}
		else {
			continuer = 0;
		}
	}
	if (current != NULL) {
		affichePlateau(window, screen, plateau, pionBlanc, pionNoir, coupJouable, current->tab, tabTemp, tabPoids, nbTour);
	}
	SDL_Delay(1000);
	freeTab(tabTemp);
	SDL_FreeSurface(plateau);
	SDL_FreeSurface(pionNoir);
	SDL_FreeSurface(pionBlanc);
	SDL_FreeSurface(coupJouable);
	return res;
}

Maillon * coup (Maillon * maillon, int ** tabTemp, int nbTour, int (*pchoix)(int,int**), int profondeur) {
	Maillon * res = NULL;
	if (!(maillon->coupsGen == NULL) && !(longListe(maillon->coupsGen) == 0)) {
		int coup = 0;
		if (maillon->coupsGen->head_->aucunCoup != 1) {
			if(pchoix == &choixAlea){
				coup = choixMinMax(maillon, nbTour, profondeur, tabTemp);
				//coup = alphaBeta(maillon, nbTour, profondeur, tabTemp);
			}
			else{
				coup = (*pchoix)(longListe(maillon->coupsGen),tabTemp);
			}
		}
		else {
			if (nbTour%2 == BLANC) {
					// printf("BLANC n'a pas pu jouer\n");
			}
			else {
					// printf("NOIR n'a pas pu jouer\n");
			}
		}
		if (coup >= 0) {
			res = maillon->coupsGen->head_;
			while (coup) {
				res = res->next_;
				coup--;
			}
		}
		else {
				// On rentre ici si coup = -1, donc si on veut quitter le jeu en cours de partie, le maillon étant déjà à NULL on a pas besoin de le modifier.
		}
	}
	else {
	}
	return res;
}

int choixAlea (int nbCoups, int ** tabPos) {
	SDL_Event event;
	int x = -1, y = -1;

	int choix = rand() % nbCoups;
	for (int i = 0; i < 200; i++) {
		SDL_PollEvent(&event);
	}
	switch (event.type) {
		case SDL_QUIT :
		choix = -1;
		break;
		case SDL_MOUSEBUTTONDOWN :
		SDL_GetMouseState(&x,&y);
		if (x > SCREEN_WIDTH*3/4) {
			if (y > SCREEN_HEIGHT/2) {
				if (y < SCREEN_HEIGHT*3/4) {
					choix = -1;
				}
			}
		}
	}
	return choix;
}

int choixJoueur(int nbCoups, int ** tabPos) {
	int choix = -1;
	int continuer = 1;
	SDL_Event event;
	int x, y;
	do {
		SDL_WaitEvent(&event);
		switch (event.type) {
			case SDL_QUIT :
			continuer = 0;
			choix = -1;
			break;
			case SDL_MOUSEBUTTONDOWN :
			SDL_GetMouseState( &x, &y );
			if (x <= SCREEN_WIDTH*3/4) {
				x = x * TaillePlateau / (SCREEN_WIDTH*3/4);
				y = y * TaillePlateau / SCREEN_HEIGHT;
				if (x > OFFSET && x < TaillePlateau - OFFSET && y > OFFSET && y < TaillePlateau - OFFSET) {
					int i = (x - OFFSET) / (TailleBloc + 2);
					int j = (y - OFFSET) / (TailleBloc + 2);
					if (i >= 0 && i < 8 && j >= 0 && j < 8) {
						choix = tabPos[j][i];
						if (choix > 0 && choix <= nbCoups) {
							choix--;
							continuer = 0;
						}
						else {
							choix = -1;
						}
					}
				}
				else {
				}
			}
			else {
				if (y > SCREEN_HEIGHT/2) {
					if (y < SCREEN_HEIGHT*3/4) {
						choix = -1;
						continuer = 0;
					}
				}
			}
			break;
			default :
			break;
		}
	} while (continuer);
	return choix;
}