/**
 * \file affichage.h
 * \brief Fonctions d'affichage des différentes parties du jeu.
 */

/**
 * \struct Coord
 * \brief Coordonnées d'un point (représentées par des entiers)
 */
struct Coord {
	int x /** Abscisse **/;
	int y /** Ordonnée **/;
};
typedef struct Coord Coord;

/**
 * \struct Bouton
 * \brief Structure représentant un bouton
 *
 * Un bouton est représenté par son coin supérieur gauche et son coin inférieur droit.
 */
struct Bouton {
	Coord coin_h_g /** Coordonnées du coin supérieur gauche **/;
	Coord coin_b_d /** Coordonnées du coin inférieur droit **/;
};
typedef struct Bouton Bouton;

/**
 * \fn int dansBouton (int x, int y, Bouton bouton)
 * \param  x      Position sur l'axe des abscisses du curseur
 * \param  y      Position sur l'axe des ordonnées du curseur
 * \param  bouton Bouton dont on veut vérifier que le curseur est dedans
 * \return        -1 si le curseur n'est pas dans le bouton, 1 s'il est dedans
 */
int dansBouton (int x, int y, Bouton bouton);

/**
 * \fn int menuPrincipal(SDL_Window * window, SDL_Renderer * screen, TTF_Font * police)
 * \brief Fonction affichant le menu principal et permettant d'intéragir avec lui.
 * 
 * \param  window Fenêtre dans laquelle on affiche
 * \param  screen Render qui sera affiché dans la fenêtre
 * \param  police Police à utiliser pour l'affichage du texte
 * \return        Le choix de l'utilisateur, 0 à 5 pour les différents types de partie, -1 s'il souhaite quitter le programme
 */
int menuPrincipal(SDL_Window * window, SDL_Renderer * screen, TTF_Font * police);

/**
 * \fn void affichePlateau (SDL_Window * window, SDL_Renderer * screen, SDL_Surface *plateau, SDL_Surface *pionBlanc, SDL_Surface *pionNoir, SDL_Surface * coupJouable, int ** tabPions, int ** tabCoups, int ** tabPoids, int nbTour)
 * \brief Fonction affichant le plateau de jeu actuel.
 * 
 * \param window      Fenêtre dans laquelle on affiche
 * \param screen      Render qui sera affiché dans la fenêtre
 * \param plateau     Surface représentant le plateau de jeu 
 * \param pionBlanc   Surface représentant un pion blanc
 * \param pionNoir    Surface représentant un pion noir
 * \param coupJouable Surface représentant un emplacement de coup jouable
 * \param tabPions    Tableau représentant le tablier de jeu
 * \param tabCoups    Tableau représentant les coups jouables pour le joueur actuel
 * \param tabPoids    Tableau contenant le poids des coups jouables
 * \param nbTour      Numéro du demi-tour actuel
 */
void affichePlateau (SDL_Window * window, SDL_Renderer * screen, SDL_Surface *plateau, SDL_Surface *pionBlanc, SDL_Surface *pionNoir, SDL_Surface * coupJouable, int ** tabPions, int ** tabCoups, int ** tabPoids, int nbTour);


/**
 * \fn int menuChoixJoueur(SDL_Window * window, SDL_Renderer * screen, TTF_Font * police)
 * \brief Fonction affichant le menu du choix de joueurs et permettant d'intéragir avec lui.
 * 
 * \param  window Fenêtre dans laquelle on affiche
 * \param  screen Render qui sera affiché dans la fenêtre
 * \param  police Police à utiliser pour l'affichage du texte
 * \return        Le choix de l'utilisateur, 1 pour ordi vs ordi, 2 pour humain vs ordi, 3 pour humain vs humain, -1 pour quitter
 */
int menuChoixJoueur(SDL_Window * window, SDL_Renderer * screen, TTF_Font * police);

/**
 * \fn int menuFin (SDL_Window * window, SDL_Renderer * ecran, TTF_Font * police, int gagnant, int score)
 * \brief Fonction affichant le menu de fin et permettant d'intéragir avec lui.
 * 
 * \param  window  Fenêtre dans laquelle on affiche
 * \param  screen  Render qui sera affiché dans la fenêtre
 * \param  police  Police à utiliser pour l'affichage du texte
 * \param  gagnant Joueur ayant gagné la partie, peut être BLANC, NOIR ou VIDE en cas d'égalité (\ref Pions)
 * \param  score   Score du joueur ayant gagné la partie, vaut 0 en cas d'égalité
 * \return         Le choix de l'utilisateur, -1 pour quitter, 1 pour retourner au menu principal
 */
int menuFin (SDL_Window * window, SDL_Renderer * ecran, TTF_Font * police, int gagnant, int score);

/**
 * \fn int menuPause (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police)
 * \brief Fonction affichant le menu de pause et permettant d'intéragir avec lui.
 * 
 * \param  window  Fenêtre dans laquelle on affiche
 * \param  screen  Render qui sera affiché dans la fenêtre
 * \param  police  Police à utiliser pour l'affichage du texte
 * \return         Le choix de l'utilisateur, -1 pour quitter, 1 pour retourner au menu principal
 */
int menuPause (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police);

/**
 * \fn int menuProfondeur (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police, int joueur)
 * \brief Fonction affichant le menu permettant de sélectionner la profondeur pour l'algorythme minmax.
 * 
 * \param  window Fenêtre dans laquelle on affiche
 * \param  screen Render qui sera affiché dans la fenêtre
 * \param  police Police à utiliser pour l'affichage du texte
 * \param  joueur Joueur pour lequel on choisit la profondeur
 * \return        Le choix de l'utilisateur supérieur à 0 ou -1 s'il souhaite quitter
 */
int menuProfondeur (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police, int joueur);