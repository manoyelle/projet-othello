#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SDL2/SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "constantes.h"
#include "affichage.h"

SDL_Color couleurBlanche = {255,255,255,255};

int dansBouton (int x, int y, Bouton bouton) {
    int dedans = -1;

    if (x > bouton.coin_h_g.x && x < bouton.coin_b_d.x) {
        if (y > bouton.coin_h_g.y && y < bouton.coin_b_d.y) {
            dedans = 1;
        }
    }
    return dedans;
}

int menuPrincipal (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police) {
    int choix = -1;
    int continuer = 1;
    int x = -1, y = -1;
    SDL_Event event;

    SDL_Rect position;
    SDL_Surface * ecran = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    SDL_Surface * classique = NULL, * fichier_1 = NULL, * fichier_2 = NULL, * fichier_3 = NULL, * fichier_4 = NULL,  * fichier_5 = NULL;
    classique = TTF_RenderText_Blended(police, "Partie classique", couleurBlanche);
    fichier_1 = TTF_RenderText_Blended(police, "Generation 1", couleurBlanche);
    fichier_2 = TTF_RenderText_Blended(police, "Generation 2", couleurBlanche);
    fichier_3 = TTF_RenderText_Blended(police, "Generation 3", couleurBlanche);
    fichier_4 = TTF_RenderText_Blended(police, "Generation 4", couleurBlanche);
    fichier_5 = TTF_RenderText_Blended(police, "Generation 5", couleurBlanche);
    if (classique == NULL || fichier_1 == NULL || fichier_2 == NULL || fichier_3 == NULL || fichier_4 == NULL || fichier_5 == NULL) {
        printf("Unable to create text surface (menuPrincipal). SDL Error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    else {
        SDL_FillRect(ecran,NULL,SDL_MapRGBA(ecran->format, 0,0,0,255));

        position.x = (SCREEN_WIDTH/2) - (classique->w/2);
        position.y = (SCREEN_HEIGHT * 1/6) - (classique->h*1.1);
        Bouton bouton_classique;
        bouton_classique.coin_h_g.x = (SCREEN_WIDTH/2) - (classique->w/2);
        bouton_classique.coin_h_g.y = (SCREEN_HEIGHT * 1/6) - (classique->h*1.1);
        bouton_classique.coin_b_d.x = bouton_classique.coin_h_g.x + classique->w;
        bouton_classique.coin_b_d.y = bouton_classique.coin_h_g.y + classique->h;
        SDL_BlitSurface(classique,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (fichier_1->w/2);
        position.y = (SCREEN_HEIGHT*2/6) - (fichier_1->h*1.1);
        Bouton bouton_fichier_1;
        bouton_fichier_1.coin_h_g.x = (SCREEN_WIDTH/2) - (fichier_1->w/2);
        bouton_fichier_1.coin_h_g.y = (SCREEN_HEIGHT*2/6) - (fichier_1->h*1.1);
        bouton_fichier_1.coin_b_d.x = bouton_fichier_1.coin_h_g.x + fichier_1->w;
        bouton_fichier_1.coin_b_d.y = bouton_fichier_1.coin_h_g.y + fichier_1->h;
        SDL_BlitSurface(fichier_1,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (fichier_2->w/2);
        position.y = (SCREEN_HEIGHT*3/6) - (fichier_2->h*1.1);
        Bouton bouton_fichier_2;
        bouton_fichier_2.coin_h_g.x = (SCREEN_WIDTH/2) - (fichier_2->w/2);
        bouton_fichier_2.coin_h_g.y = (SCREEN_HEIGHT*3/6) - (fichier_2->h*1.1);
        bouton_fichier_2.coin_b_d.x = bouton_fichier_2.coin_h_g.x + fichier_2->w;
        bouton_fichier_2.coin_b_d.y = bouton_fichier_2.coin_h_g.y + fichier_2->h;
        SDL_BlitSurface(fichier_2,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (fichier_3->w/2);
        position.y = (SCREEN_HEIGHT*4/6) - (fichier_3->h*1.1);
        Bouton bouton_fichier_3;
        bouton_fichier_3.coin_h_g.x = (SCREEN_WIDTH/2) - (fichier_3->w/2);
        bouton_fichier_3.coin_h_g.y = (SCREEN_HEIGHT*4/6) - (fichier_3->h*1.1);
        bouton_fichier_3.coin_b_d.x = bouton_fichier_3.coin_h_g.x + fichier_3->w;
        bouton_fichier_3.coin_b_d.y = bouton_fichier_3.coin_h_g.y + fichier_3->h;
        SDL_BlitSurface(fichier_3,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (fichier_4->w/2);
        position.y = (SCREEN_HEIGHT*5/6) - (fichier_4->h*1.1);
        Bouton bouton_fichier_4;
        bouton_fichier_4.coin_h_g.x = (SCREEN_WIDTH/2) - (fichier_4->w/2);
        bouton_fichier_4.coin_h_g.y = (SCREEN_HEIGHT*5/6) - (fichier_4->h*1.1);
        bouton_fichier_4.coin_b_d.x = bouton_fichier_4.coin_h_g.x + fichier_4->w;
        bouton_fichier_4.coin_b_d.y = bouton_fichier_4.coin_h_g.y + fichier_4->h;
        SDL_BlitSurface(fichier_4,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (fichier_5->w/2);
        position.y = (SCREEN_HEIGHT*6/6) - (fichier_5->h*1.1);
        Bouton bouton_fichier_5;
        bouton_fichier_5.coin_h_g.x = (SCREEN_WIDTH/2) - (fichier_5->w/2);
        bouton_fichier_5.coin_h_g.y = (SCREEN_HEIGHT*6/6) - (fichier_5->h*1.1);
        bouton_fichier_5.coin_b_d.x = bouton_fichier_5.coin_h_g.x + fichier_5->w;
        bouton_fichier_5.coin_b_d.y = bouton_fichier_5.coin_h_g.y + fichier_5->h;
        SDL_BlitSurface(fichier_5,NULL,ecran,&position);

        SDL_Texture * texture = SDL_CreateTextureFromSurface(screen, ecran);

        if (texture == NULL) {
            printf("Erreur SDL_CreateTexture (menuPrincipal) : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
        SDL_RenderClear(screen);
        SDL_RenderCopy(screen, texture, NULL, NULL);
        SDL_RenderPresent(screen);
        SDL_UpdateWindowSurface(window);

        while (continuer) {
            SDL_WaitEvent(&event);
            switch(event.type){
                case SDL_QUIT:
                continuer = 0;
                choix = -1;
                break;
                case SDL_MOUSEBUTTONDOWN :
                SDL_GetMouseState(&x, &y);
                // Partie classique
                if (dansBouton(x, y, bouton_classique) > 0) {
                    choix = 0;
                    continuer = 0;
                }
                else if (dansBouton(x, y, bouton_fichier_1) > 0) {
                    choix = 1;
                    continuer = 0;
                }
                else if (dansBouton(x, y, bouton_fichier_2) > 0) {
                    choix = 2;
                    continuer = 0;
                }
                else if (dansBouton(x, y, bouton_fichier_3) > 0) {
                    choix = 3;
                    continuer = 0;
                }
                else if (dansBouton(x, y, bouton_fichier_4) > 0) {
                    choix = 4;
                    continuer = 0;
                }
                else if (dansBouton(x, y, bouton_fichier_5) > 0) {
                    choix = 5;
                    continuer = 0;
                }
                break;
                default :
                break;
            }
        }

        SDL_FreeSurface(ecran);
        SDL_FreeSurface(classique);
        SDL_FreeSurface(fichier_1);
        SDL_FreeSurface(fichier_2);
        SDL_FreeSurface(fichier_3);
        SDL_FreeSurface(fichier_4);
        SDL_FreeSurface(fichier_5);
        SDL_DestroyTexture(texture);
    }
    return choix;
}

void affichePlateau (SDL_Window * window, SDL_Renderer * screen, SDL_Surface *plateau, SDL_Surface *pionBlanc, SDL_Surface *pionNoir, SDL_Surface * coupJouable, int ** tabPions, int ** tabCoups, int ** tabPoids, int nbTour){
    TTF_Font * police = NULL, * police_poids = NULL;
    police = TTF_OpenFont("../Polices/angelina.ttf",55);
    police_poids = TTF_OpenFont("../Polices/Rounded_Elegance.ttf", 20);
    if (police == NULL || police_poids == NULL) {
        printf("TTF_OpenFont : %s\n", TTF_GetError());
    }
    SDL_Rect position; 
    position.x = 0;
    position.y = 0;
    char str_poids[30];

    SDL_Surface * joueur = NULL, * couleur = NULL, * quitter = NULL, * sur_poids = NULL;
    joueur = TTF_RenderText_Blended(police, "Joueur :", couleurBlanche);
    if (nbTour%2 == BLANC) {
        couleur = TTF_RenderText_Blended(police, "Blanc", couleurBlanche);
    }
    else {
        couleur = TTF_RenderText_Blended(police, "Noir", couleurBlanche);
    }
    quitter = TTF_RenderText_Blended(police, "Quitter", couleurBlanche);
    if (joueur == NULL || couleur == NULL || quitter == NULL) {
        printf("Unable to create text surface (affichePlateau). TTF Error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_Surface * ecran = SDL_CreateRGBSurface(0, plateau->w, plateau->h, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    SDL_Surface * menu = SDL_CreateRGBSurface(0, SCREEN_WIDTH*1/4, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    if (ecran == NULL || menu == NULL) {
        printf("Unable to create surface. SDL Error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_BlitSurface(plateau, NULL, ecran, &position);

    for(int i = 0; i < 8; i++){
        for(int j = 0 ; j < 8; j++){
            position.x = (j * (TailleBloc+2))+OFFSET;
            position.y = (i * (TailleBloc+2))+OFFSET;
            if(tabPions[i][j] == BLANC){
                SDL_BlitSurface(pionBlanc, NULL, ecran, &position);
            }
            else if(tabPions[i][j] == NOIR){
                SDL_BlitSurface(pionNoir, NULL, ecran, &position);
            }
            else{
            }
            if (tabCoups[i][j] > 0) {
                SDL_BlitSurface(coupJouable, NULL, ecran, &position);
                sprintf(str_poids, "%d", tabPoids[i][j]);
                sur_poids = TTF_RenderText_Blended(police_poids, str_poids, couleurBlanche);
                if (sur_poids == NULL) {
                    printf("Unable to create text surface (affichePlateau). TTF Error: %s\n", TTF_GetError());
                }
                else {
                    position.x = position.x + ((TailleBloc+1)/2) - (sur_poids->w/2);
                    position.y = position.y + ((TailleBloc+1)/2) - (sur_poids->h/2);
                    SDL_BlitSurface(sur_poids, NULL, ecran, &position);
                }
            }
        }
    }

    position.x = (menu->w/2) - (joueur->w/2);
    position.y = joueur->h * 1/4;
    SDL_BlitSurface(joueur, NULL, menu, &position);

    position.x = (menu->w/2) - (couleur->w/2);
    position.y = (joueur->h*5/4) + (couleur->h*1/4);
    SDL_BlitSurface(couleur, NULL, menu, &position);

    position.x = (menu->w/2) - (quitter->w/2);
    position.y = (menu->h*5/8) - (quitter->h/2);
    SDL_BlitSurface(quitter, NULL, menu, &position);
    
    SDL_Texture * texture_menu = SDL_CreateTextureFromSurface(screen, menu);
    SDL_Texture * plateauJeu = SDL_CreateTextureFromSurface(screen, ecran);  
    if (plateauJeu == NULL || menu == NULL) {
        printf("Unable to create texture! SDL Error: %s\n", SDL_GetError() );
        exit(EXIT_FAILURE);
    }


    SDL_SetRenderDrawColor(screen, 0, 0, 0, 255);
    SDL_RenderClear(screen); 

    position.x = 0;
    position.y = 0;
    position.w = SCREEN_WIDTH*3/4;
    position.h = SCREEN_HEIGHT;
    SDL_RenderCopy(screen, plateauJeu, NULL, &position); 

    position.x = SCREEN_WIDTH*3/4;
    position.y = 0;
    position.w = SCREEN_WIDTH*1/4;
    position.h = SCREEN_HEIGHT;
    SDL_RenderCopy(screen, texture_menu, NULL, &position);

    SDL_RenderPresent(screen);
    SDL_FreeSurface(ecran); 
    SDL_FreeSurface(joueur);
    SDL_FreeSurface(couleur);
    SDL_FreeSurface(quitter);
    TTF_CloseFont(police);
    TTF_CloseFont(police_poids);
    SDL_DestroyTexture(plateauJeu);
    SDL_DestroyTexture(texture_menu);
}

int menuChoixJoueur (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police) {
    int continuer = 1;
    int choix = -1;
    int x = -1, y = -1;
    SDL_Event event;

    SDL_Surface * ecran = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);

    SDL_Rect position;
    SDL_Surface * ordi = NULL, * mixte = NULL, * humain = NULL;

    ordi = TTF_RenderText_Blended(police, "1 : ordi vs ordi", couleurBlanche);
    mixte = TTF_RenderText_Blended(police, "2 : ordi vs humain", couleurBlanche);
    humain = TTF_RenderText_Blended(police, "3 : humain vs humain", couleurBlanche);
    if (ordi == NULL || mixte == NULL || humain == NULL) {
        printf("Unable to create text surface (menuChoixJoueur). SDL Error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_FillRect(ecran,NULL,SDL_MapRGBA(ecran->format, 0,0,0,255));

    position.x = (SCREEN_WIDTH/2) - (ordi->w/2);
    position.y = (SCREEN_HEIGHT/2) - mixte->h - ordi->h;
    Bouton bouton_ordi;
    bouton_ordi.coin_h_g.x = (SCREEN_WIDTH/2) - (ordi->w/2);
    bouton_ordi.coin_h_g.y = (SCREEN_HEIGHT/2) - mixte->h - ordi->h;
    bouton_ordi.coin_b_d.x = bouton_ordi.coin_h_g.x + ordi->w;
    bouton_ordi.coin_b_d.y = bouton_ordi.coin_h_g.y + ordi->h;
    SDL_BlitSurface(ordi,NULL,ecran,&position);

    position.x = (SCREEN_WIDTH/2) - (mixte->w/2);
    position.y = (SCREEN_HEIGHT/2) - (mixte->h/2);
    Bouton bouton_mixte;
    bouton_mixte.coin_h_g.x = (SCREEN_WIDTH/2) - (mixte->w/2);
    bouton_mixte.coin_h_g.y = (SCREEN_HEIGHT/2) - (mixte->h/2);
    bouton_mixte.coin_b_d.x = bouton_mixte.coin_h_g.x + mixte->w;
    bouton_mixte.coin_b_d.y = bouton_mixte.coin_h_g.y + mixte->h;
    SDL_BlitSurface(mixte,NULL,ecran,&position);

    position.x = (SCREEN_WIDTH/2) - (humain->w/2);
    position.y = (SCREEN_HEIGHT/2) + mixte->h;
    Bouton bouton_humain;
    bouton_humain.coin_h_g.x = (SCREEN_WIDTH/2) - (humain->w/2);
    bouton_humain.coin_h_g.y = (SCREEN_HEIGHT/2) + mixte->h;
    bouton_humain.coin_b_d.x = bouton_humain.coin_h_g.x + humain->w;
    bouton_humain.coin_b_d.y = bouton_humain.coin_h_g.y + humain->h; 
    SDL_BlitSurface(humain,NULL,ecran,&position);

    SDL_Texture * texture = SDL_CreateTextureFromSurface(screen, ecran);
    if (texture == NULL) {
        printf("Erreur SDL_CreateTexture (menuChoixJoueur) : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_RenderClear(screen);
    SDL_RenderCopy(screen, texture, NULL, NULL);
    SDL_RenderPresent(screen);
    SDL_UpdateWindowSurface(window);

    while (continuer) {
        SDL_WaitEvent(&event);
        switch(event.type){
            case SDL_QUIT:
            continuer = 0;
            choix = -1;
            break;
            case SDL_MOUSEBUTTONDOWN :
            SDL_GetMouseState( &x, &y );
            if (dansBouton(x, y, bouton_ordi) > 0) {
                choix = 1;
                continuer = 0;
            }
            else if (dansBouton(x, y, bouton_mixte) > 0) {
                choix = 2;
                continuer = 0;
            }
            else if (dansBouton(x, y, bouton_humain) > 0) {
                choix = 3;
                continuer = 0;
            }
            break;
            default:
            break;
        }
    }

    SDL_FreeSurface(ecran);
    SDL_FreeSurface(ordi);
    SDL_FreeSurface(mixte);
    SDL_FreeSurface(humain);
    SDL_DestroyTexture(texture);

    return choix;
}

int menuFin (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police, int gagnant, int score) {
    int continuer = 1;
    int choix = 0;
    int x = -1, y = -1;
    SDL_Event event;

    SDL_Surface * ecran = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    if (ecran == NULL) {
        printf("Unable to create surface (menuFin). SDL Error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_Rect position;
    SDL_Surface * fin = NULL, * surface_gagnant = NULL, * surface_score = NULL, * quitter = NULL, * menu = NULL;
    char str_score[10];
    sprintf(str_score, "%d points", score);

    fin = TTF_RenderText_Blended(police, "-- Gagnant --", couleurBlanche);
    if (gagnant == BLANC) {
        surface_gagnant = TTF_RenderText_Blended(police,"BLANC", couleurBlanche);
    }
    else if(gagnant == NOIR) {
        surface_gagnant = TTF_RenderText_Blended(police,"NOIR", couleurBlanche);
    }
    else{
        surface_gagnant = TTF_RenderText_Blended(police,"EGALITE", couleurBlanche);
    }
    surface_score = TTF_RenderText_Blended(police, str_score, couleurBlanche);
    quitter = TTF_RenderText_Blended(police, "Quitter", couleurBlanche);
    menu = TTF_RenderText_Blended(police, "Retour au menu", couleurBlanche);
    if (fin == NULL || surface_gagnant == NULL || surface_score == NULL || quitter == NULL || menu == NULL) {
        printf("Erreur lors de l'initialisation des textes du menu de fin : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    else {
        SDL_FillRect(ecran,NULL,SDL_MapRGBA(ecran->format, 0,0,0,255));

        position.x = (SCREEN_WIDTH/2) - (fin->w/2);
        position.y = (SCREEN_HEIGHT/2) - fin->h - surface_gagnant->h - surface_score->h;
        SDL_BlitSurface(fin,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (surface_gagnant->w/2);
        position.y = (SCREEN_HEIGHT/2) - surface_gagnant->h - surface_score->h;
        SDL_BlitSurface(surface_gagnant,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (surface_score->w/2);
        position.y = (SCREEN_HEIGHT/2) - surface_score->h;
        SDL_BlitSurface(surface_score,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (menu->w/2);
        position.y = (SCREEN_HEIGHT*3/4) - (menu->h*1.05);
        Bouton bouton_menu;
        bouton_menu.coin_h_g.x = (SCREEN_WIDTH/2) - (menu->w/2);
        bouton_menu.coin_h_g.y = (SCREEN_HEIGHT*3/4) - (menu->h*1.05);
        bouton_menu.coin_b_d.x = bouton_menu.coin_h_g.x + menu->w;
        bouton_menu.coin_b_d.y = bouton_menu.coin_h_g.y + menu->h;
        SDL_BlitSurface(menu,NULL,ecran,&position);

        position.x = (SCREEN_WIDTH/2) - (quitter->w/2);
        position.y = (SCREEN_HEIGHT*3/4) + (quitter->h*1.05);
        Bouton bouton_quitter;
        bouton_quitter.coin_h_g.x = (SCREEN_WIDTH/2) - (quitter->w/2);
        bouton_quitter.coin_h_g.y = (SCREEN_HEIGHT*3/4) + (quitter->h*1.05);
        bouton_quitter.coin_b_d.x = bouton_quitter.coin_h_g.x + quitter->w;
        bouton_quitter.coin_b_d.y = bouton_quitter.coin_h_g.y + quitter->h;
        SDL_BlitSurface(quitter,NULL,ecran,&position);

        SDL_Texture * texture = SDL_CreateTextureFromSurface(screen,ecran);
        if (texture == NULL) {
            printf("Erreur SDL_CreateTexture (menuFin) : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }

        SDL_RenderClear(screen);
        SDL_RenderCopy(screen, texture, NULL, NULL);
        SDL_RenderPresent(screen);
        SDL_UpdateWindowSurface(window);

        while (continuer) {
            SDL_WaitEvent(&event);

            switch (event.type) {
                case SDL_QUIT :
                choix = -1;
                continuer = 0;
                break;
                case SDL_MOUSEBUTTONDOWN :
                SDL_GetMouseState( &x, &y );
                // Clique sur le Bouton retour menu
                if (dansBouton(x, y, bouton_menu) > 0) {
                    choix = 1;
                    continuer = 0;
                }
                // Clique sur le Bouton quitter
                else if (dansBouton(x, y, bouton_quitter) > 0) {
                    choix = -1;
                    continuer = 0;
                }
                break;
                default :
                break;
            }
        }

        SDL_FreeSurface(ecran);
        SDL_FreeSurface(fin);
        SDL_FreeSurface(surface_gagnant);
        SDL_FreeSurface(surface_score);
        SDL_FreeSurface(quitter);
        SDL_FreeSurface(menu);
        SDL_DestroyTexture(texture);
    }
    return choix;
}

int menuPause (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police) {
    int continuer = 1;
    int choix = 0;
    int x = -1, y = -1;
    SDL_Event event;

    SDL_Surface * ecran = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    if (ecran == NULL) {
        printf("Unable to create surface (menuPause). SDL Error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_Rect position;
    SDL_Surface * retour_menu = NULL, * quitter = NULL;
    retour_menu = TTF_RenderText_Blended(police, "Retour au menu", couleurBlanche);
    quitter = TTF_RenderText_Blended(police, "Quitter", couleurBlanche);
    if (retour_menu == NULL || quitter == NULL) {
        printf("Unable to create text surface. SDL Error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    position.x = (SCREEN_WIDTH/2) - (retour_menu->w/2);
    position.y = (SCREEN_HEIGHT*3/8) - (retour_menu->h/2);
    Bouton bouton_menu;
    bouton_menu.coin_h_g.x = (SCREEN_WIDTH/2) - (retour_menu->w/2);
    bouton_menu.coin_h_g.y = (SCREEN_HEIGHT*3/8) - (retour_menu->h/2);
    bouton_menu.coin_b_d.x = bouton_menu.coin_h_g.x + retour_menu->w;
    bouton_menu.coin_b_d.y = bouton_menu.coin_h_g.y + retour_menu->h;
    SDL_BlitSurface(retour_menu, NULL, ecran, &position);

    position.x = (SCREEN_WIDTH/2) - (quitter->w/2);
    position.y = (SCREEN_HEIGHT*6/8) - (quitter->h/2);
    Bouton bouton_quitter;
    bouton_quitter.coin_h_g.x = (SCREEN_WIDTH/2) - (quitter->w/2);
    bouton_quitter.coin_h_g.y = (SCREEN_HEIGHT*6/8) - (quitter->h/2);
    bouton_quitter.coin_b_d.x = bouton_quitter.coin_h_g.x + quitter->w;
    bouton_quitter.coin_b_d.y = bouton_quitter.coin_h_g.y + quitter->h;
    SDL_BlitSurface(quitter, NULL, ecran, &position);

    SDL_Texture * texture = SDL_CreateTextureFromSurface(screen,ecran);
    if (texture == NULL) {
        printf("Erreur SDL_CreateTexture (menuFin) : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_RenderClear(screen);
    SDL_RenderCopy(screen, texture, NULL, NULL);
    SDL_RenderPresent(screen);
    SDL_UpdateWindowSurface(window);

    while (continuer) {
        SDL_WaitEvent(&event);

        switch (event.type) {
            case SDL_QUIT :
            continuer = 0;
            choix = -1;
            break;
            case SDL_MOUSEBUTTONDOWN :
            SDL_GetMouseState(&x, &y);
            if (dansBouton(x, y, bouton_menu) > 0) {
                choix = 1;
                continuer = 0;
            }
            else if (dansBouton(x, y, bouton_quitter) > 0) {
                choix = -1;
                continuer = 0;
            }
            break;
            default :
            break;
        }
    }

    SDL_FreeSurface(ecran);
    SDL_FreeSurface(retour_menu);
    SDL_FreeSurface(quitter);
    SDL_DestroyTexture(texture);

    return choix;
}

int menuProfondeur (SDL_Window * window, SDL_Renderer * screen, TTF_Font * police, int joueur) {
    int continuer = 1;
    int max = 10, min = 1;
    int profondeur = min;
    int x = -1, y = -1;
    SDL_Rect position;
    SDL_Event event;

    TTF_Font * police_fleche = TTF_OpenFont("../Polices/Arrows.ttf", 65);
    if (police_fleche == NULL) {
        printf("Erreur chargement police (menuProfondeur). TTF error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    char str_joueur[30], str_profondeur[10];    
    if (joueur == BLANC) {
        sprintf(str_joueur, "Profondeur pour blanc");
    }
    else if (joueur == NOIR) {
        sprintf(str_joueur, "Profondeur pour noir");
    }
    else {
        printf("Valeur pour joueur invalide (menuProfondeur).\n");
        exit(EXIT_FAILURE);
    }
    sprintf(str_profondeur, "%d", profondeur);

    SDL_Surface * ecran = NULL, * sur_joueur = NULL, * sur_profondeur = NULL, * fleche_haut = NULL, * fleche_bas = NULL, * sur_ok = NULL;
    ecran = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
    sur_joueur = TTF_RenderText_Blended(police, str_joueur, couleurBlanche);
    sur_profondeur = TTF_RenderText_Blended(police, str_profondeur, couleurBlanche);
    fleche_haut = TTF_RenderText_Blended(police_fleche, "S", couleurBlanche);
    fleche_bas = TTF_RenderText_Blended(police_fleche, "T", couleurBlanche);
    sur_ok = TTF_RenderText_Blended(police, "Valider", couleurBlanche);

    if (ecran == NULL) {
        printf("Erreur création surface (menuProfondeur). SDL error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    if (sur_joueur == NULL || sur_profondeur == NULL || fleche_haut == NULL || fleche_bas == NULL || sur_ok == NULL) {
        printf("Erreur création des surfaces de texte (menuProfondeur). TTF error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    Bouton haut, bas, ok;

    haut.coin_h_g.x = (SCREEN_WIDTH/2) + (fleche_haut->w*1/4);
    haut.coin_h_g.y = (SCREEN_HEIGHT/2) - fleche_haut->h;
    haut.coin_b_d.x = haut.coin_h_g.x + fleche_haut->w;
    haut.coin_b_d.y = haut.coin_h_g.y + fleche_haut->h;

    bas.coin_h_g.x = (SCREEN_WIDTH/2) + (fleche_bas->w*1/4);
    bas.coin_h_g.y = (SCREEN_HEIGHT/2);
    bas.coin_b_d.x = bas.coin_h_g.x + fleche_bas->w;
    bas.coin_b_d.y = bas.coin_h_g.y + fleche_bas->h;

    ok.coin_h_g.x = (SCREEN_WIDTH/2) - (sur_ok->w/2);
    ok.coin_h_g.y = SCREEN_HEIGHT - (sur_ok->h*3/2);
    ok.coin_b_d.x = ok.coin_h_g.x + sur_ok->w;
    ok.coin_b_d.y = ok.coin_h_g.y + sur_ok->h;

    while (continuer) {
        sprintf(str_profondeur, "%d", profondeur);
        sur_profondeur = TTF_RenderText_Blended(police, str_profondeur, couleurBlanche);
        if (sur_profondeur == NULL) {
            printf("Erreur création surface de texte (compteur, menuProfondeur). TTF error: %s\n", TTF_GetError());
            exit(EXIT_FAILURE);
        }

        SDL_FillRect(ecran, NULL, 0x000000);

        position.x = (SCREEN_WIDTH/2) - (sur_joueur->w/2);
        position.y = (sur_joueur->h/2);
        SDL_BlitSurface(sur_joueur, NULL, ecran, &position);

        position.x = (SCREEN_WIDTH/2) - (sur_profondeur->w*5/4);
        position.y = (SCREEN_HEIGHT/2) - (sur_profondeur->h/2);
        SDL_BlitSurface(sur_profondeur, NULL, ecran, &position);

        position.x = (SCREEN_WIDTH/2) + (fleche_haut->w*1/4);
        position.y = (SCREEN_HEIGHT/2) - fleche_haut->h;
        SDL_BlitSurface(fleche_haut, NULL, ecran, &position);

        position.x = (SCREEN_WIDTH/2) + (fleche_bas->w*1/4);
        position.y = (SCREEN_HEIGHT/2);
        SDL_BlitSurface(fleche_bas, NULL, ecran, &position);

        position.x = (SCREEN_WIDTH/2) - (sur_ok->w/2);
        position.y = SCREEN_HEIGHT - (sur_ok->h*3/2);
        SDL_BlitSurface(sur_ok, NULL, ecran, &position);

        SDL_Texture * texture = SDL_CreateTextureFromSurface(screen,ecran);
        if (texture == NULL) {
            printf("Erreur SDL_CreateTexture (menuFin) : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }

        SDL_RenderClear(screen);
        SDL_RenderCopy(screen, texture, NULL, NULL);
        SDL_RenderPresent(screen);
        SDL_UpdateWindowSurface(window);
        SDL_RenderClear(screen);
        SDL_UpdateWindowSurface(window);
        SDL_DestroyTexture(texture);

        SDL_WaitEvent(&event);

        switch (event.type) {
            case SDL_QUIT :
            profondeur = -1;
            continuer = 0;
            break;
            case SDL_MOUSEBUTTONDOWN :
            SDL_GetMouseState(&x, &y);
            if (dansBouton(x, y, haut) > 0) {
                if (profondeur < max) {
                    profondeur++;
                }
            }
            else if (dansBouton(x, y, bas) > 0) {
                if (profondeur > min) {
                    profondeur--;
                }
            }
            else if (dansBouton(x, y, ok) > 0) {
                continuer = 0;
            }
            break;
            default :
            break;
        }
    }

    TTF_CloseFont(police_fleche);
    SDL_FreeSurface(ecran);
    SDL_FreeSurface(sur_joueur);
    SDL_FreeSurface(sur_profondeur);
    SDL_FreeSurface(fleche_bas);
    SDL_FreeSurface(fleche_haut);
    SDL_FreeSurface(sur_ok);
    return profondeur;
}