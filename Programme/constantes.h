/**
 *\file constantes.h
 *\brief Constantes
 *
 * Fichier contenant les constantes globales.
 */
#define SCREEN_WIDTH 640   /**< Largeur de la fenêtre **/
#define SCREEN_HEIGHT 480  /**< Hauteur de la fenêtre **/
#define TaillePlateau 343  /**< Taille d'un côté de l'image du plateau **/
#define nbCase 8           /**< Le nombre de case du plateau **/
#define TailleBloc 37      /**< Taille d'un côté d'une case du plateau **/
#define OFFSET 15		   /**< Largeur du bord du plateau **/

/**
 * \enum Pions
 * \brief Représentation des pions.
 */
enum Pions{
	BLANC /**< Pion blanc */,
	NOIR  /**< Pion noir */,
	VIDE  /**< Case vide */
};
typedef enum Pions Pions;