/**
 * \mainpage Projet de Prép'Isima 2 
 * 
 * __Tuteur :__ M. Yves-Jean Daniel   
 * __Etudiants :__ Aurélien Authier, Margot Noyelle   
 *
 * # Introduction   
 * 
 * Ce projet est un projet de deuxième année de Prép'Isima. Le but de ce 
 * projet est de programmer un jeu d'othello (ou reversi) et de programmer un 
 * algorithme de résolution de façon à ce que l'ordinateur soit capable de 
 * jouer de manière "intelligente" et puisse gagner la partie.  
 * Le langage utilisé est le C et la librairie SDL 2.0 est utilisée pour 
 * l'affichage graphique.
 *
 * *Documentation mise à jour le 01/06/2020*
 */