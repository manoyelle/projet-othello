/**
 * \file minmax.h
 * \brief Implémentation de l'algorithme minmax.
 *
 * Fonctions liées à determination des meilleurs coups possibles.
 */

/**
 * \fn int bord (Maillon * maillon)
 * \brief Fonction permettant de savoir si un le coup joué est sur un bord, dans un coin ou aucun des deux.
 * \param  maillon Le coup dont on veut savoir la position
 * @return         0 si le maillon n'est pas sur un bord ou dans un coin, 1 s'il est sur un bord et 2 s'il est dans un coin
 */
int bord (Maillon * maillon);

/**
 * \fn Liste * noteCoups (Maillon * maillon, int nbTour, int ** tabPos)
 * \brief Fonction d'attibution des notes aux coups générés à partir d'une situation donnée. Fonction d'évaluation.
 * 
 * \param  maillon  Dernier coup joué et situation de départ pour le calcul.
 * \param  nbTour   Numéro du tour actuel.
 * \param  tabPos   Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier. 
 */
void noteCoups (Maillon * maillon, int nbTour, int ** tabPos);

/**
 * \fn int choixMinMax (Maillon * maillon, int nbTour, int profondeur, int ** tabPos)
 * \brief Fonction de détermination du meilleur coup à joueur dans une situation donnée.
 *
 * Implémentation de l'algorithme minmax.
 * 
 * \param  maillon  	Dernier coup joué et situation de départ pour le calcul.
 * \param  nbTour   	Numéro du tour actuel.
 * \param  tabPos   	Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier. 
 * \param  profondeur	Profondeur de recherche de la fonction d'évaluation.
 * \return 				Le numéro du maillon à jouer ou -1 si l'utilisateur souhaite quitter.
 */
int choixMinMax (Maillon * maillon, int nbTour, int profondeur, int ** tabPos);

/**
 * \fn int remplissageMinMax (Maillon * maillon, int nbTour, int profondeur, int ** tabPos)
 * \brief Fonction de détermination du meilleur coup à joueur dans une situation donnée.
 *
 * Implémentation de l'algorithme minmax.
 * 
 * \param  maillon  	Dernier coup joué et situation de départ pour le calcul.
 * \param  nbTour   	Numéro du tour actuel.
 * \param  tabPos   	Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier. 
 * \param  profondeur   Profondeur de recherche de la fonction d'évaluation.
 */
void remplissageMinMax (Maillon * maillon, int nbTour, int profondeur, int ** tabPos);

/**
 * \fn void minMaxDown (Maillon * maillon, int nbTour, int profondeurAct, int profondeurMax, int ** tabPos)
 * \brief Fonction permettant de descendre à la profondeur voulue dans l'arbre des coups et de noter les coups à cette profondeur.
 * Cette fonction permet de noter les coups à la bonne profondeur pour pouvoir ensuite faire remonter cette note, si on rencontre une feuille avant d'atteindre
   la profondeur voulue cette feuille et ses frères sont notés.
 * 
 * \param maillon         Maillon en cours de traitement 
 * \param nbTour          Numéro du tour auquel le maillon pourrait être joué
 * \param profondeurAct   Profondeur dans l'arbre par rapport au maillon de départ à laquelle le traitement est actuellement
 * \param profondeurMax   Profondeur maximale à atteindre
 * \param tabPos          Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier. 
 */
void minMaxDown (Maillon * maillon, int nbTour, int profondeurAct, int profondeurMax, int ** tabPos);

/**
 * \fn int valMax (Maillon * maillon, int profondeur, int profondeurMax)
 * \brief  Trouve le maillon ayant le poids le plus élévé parmi les fils du maillon passé en paramètre.
 *
 * Si on a pas atteint profondeurMax cette fonction appelle valMin sur tous les fils du maillon passé en paramètre.
 * 
 * \param  maillon       Maillon dont on doit trouver le fils ayant le poids le plus élevé.
 * \param  profondeur    Profondeur auquel le traitement est actuellement.
 * \param  profondeurMax Profondeur à atteindre si possible.
 * \return               La position du maillon si profondeur est égal à 0 sinon son poids. Si le maillon n'a pas de fils la fonction renvoie le poids du maillon d'origine.
 */
int valMax (Maillon * maillon, int profondeur, int profondeurMax);

/**
 * \fn int valMin(Maillon * maillon, int profondeur, int profondeurMax)
 * \brief Trouve le maillon ayant le poids le plus faible parmi les fils du maillon passé en paramètre.
 *
 * Si on a pas atteint profondeurMax cette fonction appelle valMax sur tous les fils du maillon passé en paramètre.
 * 
 * \param  maillon       Maillon dont on doit trouver le fils ayant le poids le plus faible.
 * \param  profondeur    Profondeur auquel le traitement est actuellement.
 * \param  profondeurMax Profondeur à atteindre si possible.
 * \return               La position du maillon si profondeur est égal à 0 sinon son poids. Si le maillon n'a pas de fils la fonction renvoie le poids du maillon d'origine.
 */
int valMin(Maillon * maillon, int profondeur, int profondeurMax);

/**
 * \fn int alphaBeta(Maillon * maillon, int nbTour, int profondeur, int ** tabPos)
 * \brief Fonction de détermination du meilleur coup à jouer dans une situation donnée.
 *
 * Implémentation de l'algorithme alphabeta.
 * 
 * \param  maillon  Dernier coup joué et situation de départ pour le calcul.
 * \param  nbTour   Numéro du tour actuel.
 * \param  tabPos   Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier. 
 * \param profondeur	Profondeur de recherche de la fonction d'évaluation.
 */
int alphaBeta(Maillon * maillon, int nbTour, int profondeur, int ** tabPos);

/**
 * \fn int Max(int a, int b)
 * \brief Retounre la valeur maximale entre deux entiers donnés.
 */
int Max(int a, int b);

/**
 * \fn int Min(int a, int b)
 * \brief Retourne la valeur minimale entre deux entiers donnés.
 */
int Min(int a, int b);