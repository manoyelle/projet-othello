/**
 * \file arbre.h
 * \brief Gestion de l'arbre des coups
 *
 * Fonctions de gestion des listes constituant l'arbre, des champs des maillons, génération de l'arbre.
 */

/**
 * \struct Maillon
 * \brief Objet maillon d'une liste doublement chainée, ou d'un arbre
 */
struct Maillon {
	struct Maillon * next_     /** Adresse du maillon précédent ou null si c'est le premier maillon de la liste ou qu'il n'a pas encore été affecté à une liste **/;
	struct Maillon * prec_     /** Adresse du maillon suivant ou null si il est le dernier maillon ou n'a pas encore été affecté **/;
	struct Maillon * origine_  /** Adresse du maillon à partir duquel le maillon actuel a été généré ou null pour la racine de l'arbre **/;
	int ** tab                 /** Le tablier au début du tour actuel, le tableau contient des entiers qui sont définis par \ref Pions **/;
	int aucunCoup              /** Vaut 0 si un coup a été joué au demi tour précédent, 1 si aucun coup n'a été joué au demi tour précédent **/;
	int coordx                 /** Abcsisse du coup qui a été joué pour obtenir cette situation **/;
	int coordy                 /** Ordonnée du coup qui a été joué pour obtenir cette situation **/;
	int poids                  /** Poids du maillon **/;
	struct Liste * coupsGen    /** La liste des coups possibles à partir du maillon actuel. Ce sont les fils du noeud actuel **/;
};
typedef struct Maillon Maillon;

/**
 * \struct Liste
 * \brief Liste doublement chainée
 * 
 * Si nb_elements_ est égal à 0 (head_ et tail_ pointeront sur null) alors la liste est vide.
 */
struct Liste{
	int nb_elements_   /** Le nombre de maillon de la liste **/;
	Maillon * head_    /** Adresse du premier maillon maillon de la liste **/;
	Maillon * tail_    /** Adresse du dernier maillon de la liste **/;
};
typedef struct Liste Liste;

/**
 * \fn Liste * creerListe ()
 * \brief Fonction de création d'une nouvelle instance de Liste.
 * 
 * \param . Ne prends aucun paramètre.
 * \return Instance nouvellement allouée de type Liste.
 */
Liste * creerListe (); 

/**
 * \fn int longListe (Liste * liste)
 * \brief Renvoie la longueur d'une liste.
 *
 * \param liste Pointeur sur une instance de type Liste.
 * \return Le nombre de maillon de la liste.
 */
int longListe (Liste * liste);

/**
 * \fn void addTail (Liste * liste, Maillon * maillon)
 * \brief Ajout d'un maillon à la fin de la liste.
 * 
 * \param[in,out] liste Pointeur sur une instance de type Liste.
 * \param maillon Pointeur sur une instance de type Maillon à ajouter à la liste.
 */
void addTail (Liste * liste, Maillon * maillon);

/**
 * \fn void addHead (Liste * liste, Maillon * maillon)
 * \brief Ajout d'un maillon au début de la liste.
 * 
 * \param[in,out] liste Pointeur sur une instance de type Liste.
 * \param maillon Pointeur sur une instance de type Maillon à ajouter à la liste.
 */
void addHead (Liste * liste, Maillon * maillon);

/**
 * \fn Maillon * removeMaillon (Liste * liste, Maillon * maillon) 
 * \brief Suppression d'un maillon
 *
 * \param  liste   Liste dans laquelle on souhaite enlever le maillon
 * \param  maillon Maillon à supprimer
 * \return         Maillon supprimé
 */
Maillon * removeMaillon (Liste * liste, Maillon * maillon);
// int valHead(Liste * liste);
// int valTail(Liste * liste);

/**
 * \fn void lectureListe (Liste * liste)
 * \brief Fonction de lecture de liste.
 * 
 * Cette fonction parcourt liste et affiche pour chaque maillon le tablier qu'il contient ainsi que si un coup a été joué au demi tour précédent et le nombre de coups possibles pour le joueur actuel.
 * 
 * \param liste Pointeur sur une instance de type Liste.
 */
void lectureListe (Liste * liste);

/**
 * \fn void genListe (int nbcoups, int nbTour, int joueur, Maillon * origine, int ** tabPos)
 * \brief Fonction remplissant le champ coupsGen d'un maillon (\ref Maillon).
 * 
 * Cette fonction va générer la liste des coups possibles pour le joueur actuel à partir du tablier d'un maillon.
 * 
 * \param  nbCoups 	 Le nombre de coups possibles pour le joueur actuel.
 * \param  nbTour 	 Le numero du tour.
 * \param  joueur 	 Le joueur actuel, peut être BLANC ou NOIR (\ref Pions).
 * \param  origine 	 Pointeur sur le maillon dont on veut remplir le champ coupsGen, donc la situation d'origine pour le tour actuel.
 * \param  tabPos 	 Tableau de même taille que le tablier remplie de 0 et de 1. Si une case vaut 1 alors un coup peut être joué à cet endroit.
 */
void genListe (int nbCoups, int nbTour, int joueur, Maillon * origine, int ** tabPos);

/**
 * \fn void freeListe (Liste * liste)
 * \brief Libère l'espace mémoire alloué à une liste et à l'intégralité de ses maillons.
 * 
 * Cette fonction libère pour chaque maillon le tableau qu'il contient, la liste coupsGen ainsi que le maillon lui-même, puis libère la liste.
 * 
 * \param liste Pointeur sur une instance de type Liste.
 */
void freeListe (Liste * liste);
// void freeMaillon (Maillon * maillon);

/**
 * \fn void trouverCoups (Maillon * maillon, int nTour, int ** tabPos)
 * \brief Fonction qui trouve les coups possibles pour le joueur actuel et remplie le champ coupsGen (\ref Maillon).
 * 
 * Cette fonction va chercher le nombre de coups possibles pour le joueur actuel et remplir un tableau contenant les emplacements des coups possibles puis il va appeler la fonction \ref genListe.
 *
 * \param maillon Maillon de la situation d'origine. Le champ tab est passé en paramètre à la fonction \ref genListe.
 * \param nTour Numéro du demi tour actuel, permet de déduire le joueur actuel.
 * \param[in,out] tabPos Tableau de même taille que le tablier dont toutes les cases sont réinitialisées à 0 au début de la fonction. Si un coup est possible à la position (x,y) alors la valeur de tabPos[x][y] passera à 1. Ce tableau est passé en paramètre à la fonction \ref genListe. 
 */
void trouverCoups (Maillon * maillon, int nTour, int ** tabPos);

/**
 * \fn void partie (SDL_Window * window, SDL_Renderer * screen, Maillon * current, int ** tabPos, int (*pchoix1)(int, int**), int (*pchoix2)(int, int**), int profondeurB, int profondeurN)
 * \brief Fonction permettant de jouer une partie et de retourner le vainqueur.
 *
 * \param window  		Surface à "remplir".
 * \param screen  		Render qui sera projeté à l'écran
 * \param current 		Adresse du maillon représentant la situation de départ
 * \param[in,out] 		tabPos Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier 
 * \param[in,out] 		pchoix1 Pointeur sur la fonction de choix du coup du joueur BLANC
 * \param[in,out] 		pchoix2 Pointeur sur la fonction de choix du coup du joueur NOIR
 * \param profondeurB   Prodondeur pour la fonction d'évaluation utilisée pour l'attribution des notes des coups blancs.
 * \param profondeurN   Prodondeur pour la fonction d'évaluation utilisée pour l'attribution des notes des coups noirs.
 *
 * \return        		Le score du joueur BLANC (négatif s'il a perdu), si le score n'est pas compris entre 64 et -64 inclus alors l'utilisateur souhaite quitter sans finir la partie.
 */
int partie (SDL_Window * window, SDL_Renderer * screen, Maillon * current, int ** tabPos, int (*pchoix1)(int, int**), int (*pchoix2)(int, int**), int profondeurB, int profondeurN);

/**
 * \fn Maillon * coup(Maillon * maillon, int ** tabPos, int nbTour,  int (*pchoix)(int,int**), int profondeur)
 * \brief Fonction retournant le maillon choisit par le joueur actuel (ordinateur ou joueur humain).
 *
 * \param maillon 		Adresse du maillon de la situation d'origine avant que le coup ne soit joué.
 * \param[in,out] 		tabTemp Tableau de 8x8 (même taille que le tablier) remplie d'entiers >= 0, si la valeur d'une case est supérieure à 0 alors un coup peut être joué à cette même position sur le tablier.
 * \param nbTour  		Numéro du demi-tour actuel.
 * \param pchoix  		Pointeur sur la fonction de choix du coup.
 * \param profondeur  	Prodondeur pour la fonction d'évaluation utilisée pour l'attribution des notes des coups.
 *
 * \return 				L'adresse du maillon du coup joué si un coup était possible, NULL dans le cas où on souhaite quitter avant la fin de la partie.
 */
Maillon * coup(Maillon * maillon, int ** tabTemp, int nbTour,  int (*pchoix)(int,int**), int profondeur);

/**
 * \fn int choixAlea (int nbCoups, int ** tabPos)
 * \brief Fonction modélisant un choix aléatoire parmis les coups possibles.
 *
 * \param nbCoups Nombre de coup possible
 * \param tabPos Tableau représentant la position des coups possibles
 * \return        Le choix aléatoire (0 à nbCoups exclu) ou -1 pour quitter
 */
int choixAlea (int nbCoups, int ** tabPos);

/**
 * \fn int choixJoueur (int nbCoups, int ** tabPos)
 * \brief Fonction permettant de récupérer le choix d'un joueur parmis les coups possibles.
 *
 * \param nbCoups Nombre de coup possible
 * \param tabPos  Tableau représentant la position des coups possibles
 * \return        Le choix du joueur (0 à nbCoups non inclus) ou -1 pour quitter
 */
int choixJoueur(int nbCoups, int ** tabPos);